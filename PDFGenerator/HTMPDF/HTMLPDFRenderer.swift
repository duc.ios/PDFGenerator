//
//  HTMLPDFRenderer.swift
//  HTMLPDFRenderer
//
//  Copyright © 2015 Aleksandar Vacić, Radiant Tap
//	https://github.com/radianttap/HTML2PDFRenderer
//
//  MIT License · http://choosealicense.com/licenses/mit/
//

import UIKit
import WebKit
import EasyPeasy

public protocol HTML2PDFRendererDelegate: class {
    func html2pdfRenderer(_ renderer: HTML2PDFRenderer, didCreatePDFAtFileURL url: URL)
    func html2pdfRenderer(_ renderer: HTML2PDFRenderer, didFailedWithError error: Error)
}

///	Uses UIPrintPageRenderer to create PDF file out of HTML web page loaded in WKWebView.
///
/// See `PaperSize` enum for declaration of supported pages. Extend as needed.
public final class HTML2PDFRenderer {
    static var shared = HTML2PDFRenderer()
    
    public typealias Callback = (URL?, Error?) -> Void
    
    //	Internal
    
    private var webView: WKWebView?
    private var webLoadingTimer: Timer?
}

private extension HTML2PDFRenderer {
    ///	UIPrintPageRenderer attributes are read-only, but they can be set using KVC.
    ///	Thus modeling those attributes with this enum.
    enum Key: String {
        case paperRect
        case printableRect
    }
}

extension HTML2PDFRenderer {
    ///	Takes the given `htmlURL`, creates hidden `WKWebView`, waits for the web page to load,
    ///	then calls the other method below.
    ///
    ///	Supports both http and file URLs.
    public func render(htmlURL: URL,
                       toPDF pdfURL: URL,
                       paperSize: PaperSize,
                       paperMargins: UIEdgeInsets = .zero,
                       delegate: HTML2PDFRendererDelegate? = nil,
                       callback: @escaping Callback = {_, _ in})
    {
        guard let webView = initWebview() else { return }
        
        if htmlURL.isFileURL {
            self.webView?.loadFileURL(htmlURL, allowingReadAccessTo: htmlURL)
        } else {
            let req = URLRequest(url: htmlURL)
            self.webView?.load(req)
        }
        
        waitForloading(webView: webView, toPDF: pdfURL, paperSize: paperSize, paperMargins: paperMargins, delegate: delegate, callback: callback)
    }
    
    public func render(htmlString: String,
                       baseURL: URL,
                       toPDF pdfURL: URL,
                       paperSize: PaperSize,
                       paperMargins: UIEdgeInsets = .zero,
                       delegate: HTML2PDFRendererDelegate? = nil,
                       callback: @escaping Callback = {_, _ in})
    {
        
        guard let webView = initWebview() else { return }
        
        self.webView?.loadHTMLString(htmlString, baseURL: baseURL)
        
        waitForloading(webView: webView, toPDF: pdfURL, paperSize: paperSize, paperMargins: paperMargins, delegate: delegate, callback: callback)
    }
    
    func initWebview() -> WKWebView? {
        guard let w = UIApplication.shared.keyWindow else { return nil }
        
        let view = UIView(frame: w.bounds)
        view.alpha = 0
        w.addSubview(view)
        
        let webView = WKWebView(frame: view.bounds)
        webView.configuration.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        view.addSubview(webView)
        self.webView = webView
        
        return webView
    }
    
    func waitForloading(webView: WKWebView,
                        toPDF pdfURL: URL,
                        paperSize: PaperSize,
                        paperMargins: UIEdgeInsets = .zero,
                        delegate: HTML2PDFRendererDelegate? = nil,
                        callback: @escaping Callback = {_, _ in})
    {
        webLoadingTimer = Timer.every(0.5, {
            [weak self] timer in
            guard let self = self else { return }
            
            if self.webView?.isLoading ?? false { return }
            timer.invalidate()
            
            self.render(webView: webView, toPDF: pdfURL, paperSize: paperSize, paperMargins: paperMargins, delegate: delegate) {
                [weak self] pdfURL, pdfError in
                guard let self = self else { return }
                
                self.webView?.superview?.removeFromSuperview()
                self.webView = nil
                
                callback(pdfURL, pdfError)
            }
        })
    }
    
    ///	Takes an existing instance of `WKWebView` and prints into paged PDF with specified paper size.
    ///
    ///	You can supply `delegate` and/or `callback` closure.
    ///	Both will be called and given back the file URL where PDF is created or an Error.
    public func render(webView: WKWebView,
                       toPDF pdfURL: URL,
                       paperSize: PaperSize,
                       paperMargins: UIEdgeInsets = .zero,
                       delegate: HTML2PDFRendererDelegate? = nil,
                       callback: Callback = {_, _ in})
    {
        let fm = FileManager.default
        if !fm.lookupOrCreate(directoryAt: pdfURL.deletingLastPathComponent()) {
            print("Can't access PDF's parent folder:\n\( pdfURL.deletingLastPathComponent() )")
            return
        }
        
        let renderer = CustomPrintPageRenderer()
        renderer.margins = paperMargins
        renderer.addPrintFormatter(webView.viewPrintFormatter(), startingAtPageAt: 0)
        
        let paperRect = CGRect(x: 0, y: 0, width: paperSize.size.width, height: paperSize.size.height)
        renderer.setValue(paperRect, forKey: Key.paperRect.rawValue)
        
        var printableRect = paperRect
        printableRect.origin.x += paperMargins.left
        printableRect.origin.y += paperMargins.top
        printableRect.size.width -= (paperMargins.left + paperMargins.right)
        printableRect.size.height -= (paperMargins.top + paperMargins.bottom)
        renderer.setValue(printableRect, forKey: Key.printableRect.rawValue)
        
        let pdfData = renderer.makePDF()
        
        do {
            try pdfData.write(to: pdfURL, options: .atomicWrite)
            print("Generated PDF file at url:\n\( pdfURL.path )")
            
            delegate?.html2pdfRenderer(self, didCreatePDFAtFileURL: pdfURL)
            callback(pdfURL, nil)
            
        } catch let error {
            print("Failed to create PDF:\n\( error )")
            
            delegate?.html2pdfRenderer(self, didFailedWithError: error)
            callback(nil, error)
            return
        }
        
    }
}

class CustomPrintPageRenderer: UIPrintPageRenderer {
    var margins = UIEdgeInsets.zero
    
    override func drawHeaderForPage(at pageIndex: Int, in headerRect: CGRect) {
        // Header
        let header = Bundle.main.loadNibNamed("Header", owner: nil, options: nil)?.first as! UIView
        header.backgroundColor = .red
        header.layoutIfNeeded()
        header.layer.render(in: UIGraphicsGetCurrentContext()!)
//        header.renderToImage(afterScreenUpdates: true).draw(in: headerRect)
    }
    
    override func drawFooterForPage(at pageIndex: Int, in footerRect: CGRect) {
        // Footer
    }
    
    func makePDF() -> Data {
        let data = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(data, paperRect, nil)
        prepare(forDrawingPages: NSMakeRange(0, numberOfPages))
        let bounds = UIGraphicsGetPDFContextBounds()
        
        for i in 0 ..< numberOfPages {
            UIGraphicsBeginPDFPage()
            drawPage(at: i, in: bounds)
            drawHeaderForPage(at: i, in: CGRect(x: 0, y: 0, width: bounds.width, height: 83))
            drawFooterForPage(at: i, in: CGRect(x: 0, y: bounds.height-83, width: bounds.width, height: 83))
        }
        UIGraphicsEndPDFContext()
        
        return data as Data
    }
}

extension String {
    
    func getTextSize(font: UIFont?, textAttributes: [NSAttributedString.Key: Any]? = nil, width: CGFloat) -> CGSize {
        let testLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        if let attributes = textAttributes {
            testLabel.attributedText = NSAttributedString(string: self, attributes: attributes)
        } else {
            testLabel.text = self
            testLabel.font = font
        }
        
        testLabel.sizeToFit()
        
        return testLabel.frame.size
    }
    
}

extension HTML2PDFRenderer {
    func renderForm(htmlURL: URL, pdfURL: URL, delegate: HTML2PDFRendererDelegate?) {
        HTML2PDFRenderer.shared.render(htmlURL: htmlURL,
                                       toPDF: pdfURL,
                                       paperSize: .a4,
                                       paperMargins: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8),
                                       delegate: delegate)
    }
    
}

extension UIView {
    @available(iOS 10.0, *)
    public func renderToImage(afterScreenUpdates: Bool = false) -> UIImage {
        let rendererFormat = UIGraphicsImageRendererFormat.default()
        rendererFormat.opaque = isOpaque
        let renderer = UIGraphicsImageRenderer(size: bounds.size, format: rendererFormat)
        
        let snapshotImage = renderer.image { _ in
            drawHierarchy(in: bounds, afterScreenUpdates: afterScreenUpdates)
        }
        return snapshotImage
    }
}
