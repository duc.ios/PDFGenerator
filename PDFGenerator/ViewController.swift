//
//  ViewController.swift
//  PDFGenerator
//
//  Created by Duc Nguyen on 14/1/2019.
//  Copyright © 2019 Duc Nguyen. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        didTapOpen(nil)
    }
    
    var renderer = HTML2PDFRenderer()

    @IBAction func didTapGenerate(_ sender: Any?) {
        createPDF()
    }
    
    @IBAction func didTapOpen(_ sender: Any?) {
        guard let htmlPath = Bundle.main.path(forResource: "HTML/FormA/exampleA.html", ofType: nil) else { return }
        
        let pdfURL = FileManager.default.documentsURL.appendingPathComponent("exampleA.pdf")
        print(htmlPath)
        renderer.render(htmlURL: URL(fileURLWithPath: htmlPath), toPDF: pdfURL, paperSize: .a4, paperMargins: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8))
    }
    
    func createPDF() {
        let pdfURL = FileManager.default.documentsURL.appendingPathComponent("exampleA.pdf")
        
        renderer.render(webView: webView, toPDF: pdfURL, paperSize: .a4, paperMargins: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8)) {
            url, error in
            print(pdfURL)
            UIPasteboard.general.string = pdfURL.absoluteString
        }
    }
}
